import React, { Component } from "react";
import { LinearGradient } from 'expo-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      bookmark:false,
      fav:false,
      filter1:'1br',
      filter2:'1bh',
    }
  }

  checkBookmark =() =>{
    this.setState({bookmark:!this.state.bookmark})
  }
  checkFav =() =>{
    this.setState({fav:!this.state.fav})
  }

  onProductClick =() =>{
    this.props.navigation.navigate('ProductDetailScreen');
  }


  getData(data, fieldName){
    if(fieldName === "filter1"){
      this.setState({filter1:data})
    }else if(fieldName === "filter2"){
      this.setState({filter2:data})
    }
  }


  // CALLING METHOD AFTER RENDER ELEMENTS 
  componentDidMount(){

  }

  
    // RENDERING DESIGNED SCREEN ELEMENTS 
    render() {
      const filter1=[
        {
          value:'1br'
        },{
          value:'2br'
        },
        {
          value:'3br'
        }
      ]
      const filter2=[
        {
          value:'1bh'
        },{
          value:'2bh'
        },
        {
          value:'3bh'
        }
      ]
      return (
        <View style={styles.container}>
          <LinearGradient
              colors={['#dad8d2', '#ffffff', '#ffffff']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{height:'100%', position:'absolute', width:'100%'}}
            />
                <ScrollView
                  style={styles.containerscroll}
                  contentContainerStyle={styles.contentContainer}>
                  <View style={styles.welcomeContainer}>
                    <View style={[styles.propertyName, styles.borderStyle]}>
                      <Text style={styles.textStyle}>Brooklyn, NY</Text>
                    </View>
                    <View style={styles.productProperties}>
                       <View style={[styles.productPrice, styles.borderStyle]}>
                          <Text style={styles.textStyle}>$0-$2500</Text>
                       </View>
                       <View style={styles.spaceCenter}></View>
                       <View style={[styles.productspecification, styles.borderStyle, {padding:0}]}>
                          <Dropdown
                            data={filter1}
                            dropdownPosition={1}
                            style={styles.textStyle}
                            dropdownOffset = {{ top: 0, left: 0 }}
                            onChangeText={(value) => this.getData(value, "filter1")}
                            value={this.state.filter1}
                            inputContainerStyle={{ borderBottomWidth: 0, marginBottom:0, paddingBottom:0, paddingTop:5, paddingLeft:5 }}
                            // error = {this.state.userRoleError}
                          />
                       </View>
                       <View style={styles.spaceCenter}></View>
                       <View style={[styles.productspecification, styles.borderStyle, {padding:0}]}>
                       <Dropdown
                            data={filter2}
                            dropdownPosition={1}
                            style={styles.textStyle}
                            dropdownOffset = {{ top: 0, left: 0 }}
                            onChangeText={(value) => this.getData(value, "filter2")}
                            value={this.state.filter1}
                            inputContainerStyle={{ borderBottomWidth: 0, marginBottom:0, paddingBottom:0, paddingTop:5, paddingLeft:5 }}
                            // error = {this.state.userRoleError}
                          />
                       </View>
                    </View>
                    <TouchableOpacity  underlayColor='transparent' style={{flex:1, width:'100%',alignItems:'center', marginTop:20}}  onPress={this.onProductClick}>
                      <View style={styles.product_allView}>
                        <View style = {styles.backgroundContainer}>
                          <Image source = {require('../assets/images/product_1.jpg')} style = {styles.backdrop} />
                        </View>
                          <View  >
                              {
                                (this.state.bookmark)?(
                                  <TouchableOpacity  underlayColor='transparent'   onPress={this.checkBookmark} style = {styles.bookmarIcon}>
                                    <Image  style = {styles.bookmarIcon} source = {require('../assets/images/bookmarked.png')} />
                                  </TouchableOpacity>
                                ):(
                                  <TouchableOpacity  underlayColor='transparent'   onPress={this.checkBookmark} style = {styles.bookmarIcon}>
                                    <Image  style = {styles.bookmarIcon} source = {require('../assets/images/bookmark.png')} />
                                  </TouchableOpacity>
                                )
                              }
                          </View>
                      </View>
                      <LinearGradient
                        colors={['#dad8d2', '#ffffff', '#ffffff']}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        style={[styles.product_detail]}
                      >
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View>
                              <Text style={styles.product_location}> WilliamsBurg</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.propertyPrice}>$1,200</Text>
                            </View>
                          </View>
                          <View style={styles.propertyAddressView}>
                            <Text style={styles.propertyAddress}>14 Dunham PI #3G</Text>
                          </View>
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View  style={styles.propertyamenitiesView}>
                              <Text style={styles.propertyamenities}> 1bed 1.5bath</Text>
                            </View>
                            <View style={styles.propertybookmarkView} >
                              {
                                (this.state.fav)?(
                                    <TouchableOpacity  underlayColor='transparent' onPress={this.checkFav} style = {styles.favicon}>
                                      <Image onPress={this.checkFav} style = {styles.favicon} source = {require('../assets/images/fav_icon.png')} />
                                    </TouchableOpacity>
                                ):(
                                  <TouchableOpacity  underlayColor='transparent' onPress={this.checkFav} style = {styles.favicon}>
                                      <Image onPress={this.checkFav} style = {styles.favicon} source = {require('../assets/images/fav.png')} />
                                  </TouchableOpacity>
                                )
                              }
                                
                            </View>
                          </View>
                          </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity  underlayColor='transparent' style={{flex:1, width:'100%',alignItems:'center', marginTop:20}}  onPress={this.onProductClick}>
                      <View style={styles.product_allView}>
                        <View style = {styles.backgroundContainer}>
                          <Image source = {require('../assets/images/product_2.jpg')} style = {styles.backdrop} />
                        </View>
                        <View >
                          <Image style = {styles.bookmarIcon} source = {require('../assets/images/bookmark.png')} />
                        </View>
                      </View>
                     
                      <LinearGradient
                        colors={['#dad8d2', '#ffffff', '#ffffff']}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        style={[styles.product_detail]}
                      >
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View>
                              <Text style={styles.product_location}> WilliamsBurg</Text>
                            </View>
                            <View style={{flex:1}}>
                              <Text style={styles.propertyPrice}>$1,200</Text>
                            </View>
                          </View>
                          <View style={{justifyContent:'flex-start', paddingLeft:20,}}>
                            <Text style={styles.propertyAddress}>14 Dunham PI #3G</Text>
                          </View>
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View  style={styles.propertyamenitiesView}>
                              <Text style={styles.propertyamenities}> 1bed 1.5bath</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image style = {styles.favicon} source = {require('../assets/images/fav.png')} />
                            </View>
                          </View>
                          </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity  underlayColor='transparent' style={{flex:1, width:'100%',alignItems:'center', marginTop:20}}  onPress={this.onProductClick}>
                      <View style={styles.product_allView}>
                        <View style = {styles.backgroundContainer}>
                          <Image source = {require('../assets/images/product_3.jpg')} style = {styles.backdrop} />
                        </View>
                        <View >
                          <Image style = {styles.bookmarIcon} source = {require('../assets/images/bookmark.png')} />
                        </View>
                      </View>
                      <LinearGradient
                        colors={['#dad8d2', '#ffffff', '#ffffff']}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        style={[styles.product_detail]}
                      >
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View>
                              <Text style={styles.product_location}> WilliamsBurg</Text>
                            </View>
                            <View style={{flex:1}}>
                              <Text style={styles.propertyPrice}>$1,200</Text>
                            </View>
                          </View>
                          <View style={{justifyContent:'flex-start', paddingLeft:20,}}>
                            <Text style={styles.propertyAddress}>14 Dunham PI #3G</Text>
                          </View>
                          <View style={{flexDirection:'row',flex:1, }}>
                            <View  style={styles.propertyamenitiesView}>
                              <Text style={styles.propertyamenities}> 1bed 1.5bath</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image style = {styles.favicon} source = {require('../assets/images/fav.png')} />
                            </View>
                          </View>
                          </LinearGradient>
                    </TouchableOpacity>
                    </View>
          
                </ScrollView>
        </View>
      );
    }

  // USING navigationOptions TO SET HEADER OF SCREEN
  static navigationOptions = ({navigation}) => {
    return{
      header: null,
    }
  }
 
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerscroll:{
    padding:15
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  textStyle:{
    fontWeight:'bold',
    color:'#727272'
  },
  borderStyle:{
    borderColor:'#c3c3c3',
    borderRadius:5,
    borderWidth:2,
    padding:10,
  },
  propertyName:{
    flex:1,
    width:'100%',
    justifyContent:'center',
    
  },
  product_view:{
    borderRadius:3,
    borderColor:'#000',
    borderWidth:1,
    flex:1,
    position:'relative',
    width:'100%'
  },
  product_allView:{
    position:'relative',
    height:250, 
    flex:1, 
    flexDirection:'column', 
    width:'100%',
  },
  product_location:{
    paddingLeft:5, 
    paddingRight:5,
    paddingTop:3, 
    paddingBottom:3,
    backgroundColor:'green', 
    color:'#fff', 
    borderRadius:2, 
    fontSize:12 
  },
  propertyPrice:{
    textAlign:'right', 
    fontSize:20, 
    fontWeight:'bold'
  },
  propertyAddressView:{
    justifyContent:'flex-start',
     paddingLeft:20
  },
  propertybookmarkView:{
    flex:1,
    justifyContent:'center'
  },
  propertyAddress:{
    fontSize:20, 
    fontWeight:'bold'
  },
  propertyamenities:{
  color:'#c2c2c4',
  fontSize:14,
  fontWeight:'bold'
},
propertyamenitiesView:{
  justifyContent:'flex-end',
   paddingLeft:20,
   flex:1
  },
  product_detail:{
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 10,
      },
    }),
    height:100,
    borderRadius:5,
    marginTop:-50,
    width:'90%',
    padding:10,
    
  },

  backgroundContainer: {
    position: 'absolute',
    height:250,
    width:'100%'
  },
  bookmarIcon: {
    width: 32,
    height: 32, 
    alignSelf:'flex-end',
    marginRight:'5%',
    marginTop:'5%',
    backgroundColor: 'transparent'
  },
  favicon:{
    width: 24,
    height: 24, 
    alignSelf:'flex-end'
  },
  backdrop: {
    flex:1,
    borderRadius:10,
    width:'100%',
    height:'100%'
  },

  productProperties:{
    flex:1,
    flexDirection:'row',
    marginTop:10
  },
  productPrice:{
    flex:0.35,
  },
  productspecification:{
    flex:0.275,
  },
  spaceCenter:{
    flex:0.05,
  },


});
