import React, { Component } from "react";
import { LinearGradient } from 'expo-linear-gradient';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
const screenHeight = Math.round(Dimensions.get('window').height);
import ViewMoreText from 'react-native-view-more-text';

export default class ProductDetailScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      bookmark: false,
      fav: false,
    }
  }

  checkBookmark = () => {
    this.setState({ bookmark: !this.state.bookmark })
  }
  checkFav = () => {
    this.setState({ fav: !this.state.fav })
  }

  backmove = () => {
    this.props.navigation.navigate('Home');
  }
  



  // CALLING METHOD AFTER RENDER ELEMENTS 
  componentDidMount() {

  }


  // RENDERING DESIGNED SCREEN ELEMENTS 
  render() {

    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.containerscroll}
          contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <View style={{ flex: 1, width: '100%', alignItems: 'center', marginTop: 20 }}>
              <View style={styles.product_allView}>
                <View style={styles.backgroundContainer}>
                  <Image source={require('../assets/images/product_3.jpg')} style={styles.backdrop} />
                </View>
                <View  >
                    <TouchableOpacity underlayColor='transparent' onPress={this.backmove} style={styles.backbutton}>
                        <Image style={styles.backbutton} source={require('../assets/images/back.png')} />
                    </TouchableOpacity>
                  {
                    (this.state.bookmark) ? (
                      <TouchableOpacity underlayColor='transparent' onPress={this.checkBookmark} style={styles.bookmarIcon}>
                        <Image style={styles.bookmarIcon} source={require('../assets/images/bookmarked.png')} />
                      </TouchableOpacity>
                    ) : (
                        <TouchableOpacity underlayColor='transparent' onPress={this.checkBookmark} style={styles.bookmarIcon}>
                          <Image style={styles.bookmarIcon} source={require('../assets/images/bookmark.png')} />
                        </TouchableOpacity>
                      )
                  }
                </View>
              </View>
              <LinearGradient
                colors={['#dad8d2', '#ffffff', '#ffffff']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={[styles.product_detail]}
              >
                <View style={{ flexDirection: 'row', flex: 1, }}>
                  <View>
                    <Text style={styles.product_location}> Bushwick</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.propertyPrice}>$1,440</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', flex: 1, paddingTop: 20 }}>
                  <View style={styles.propertyAddressView}>
                    <Text style={styles.propertyAddress}>17 Troutman St </Text>
                  </View>
                  <View style={styles.propertybookmarkView} >

                    {
                      (this.state.fav) ? (
                        <TouchableOpacity underlayColor='transparent' onPress={this.checkFav} style={styles.favicon}>
                          <Image onPress={this.checkFav} style={styles.favicon} source={require('../assets/images/heart_detail_page_fav.png')} />
                        </TouchableOpacity>
                      ) : (
                          <TouchableOpacity underlayColor='transparent' onPress={this.checkFav} style={styles.favicon}>
                            <Image onPress={this.checkFav} style={styles.favicon} source={require('../assets/images/heart_detail_page.png')} />
                          </TouchableOpacity>
                        )
                    }

                  </View>
                </View>

                <View style={{ flexDirection: 'row', flex: 1 }}>

                  <ScrollableTabView
                    tabBarPosition='bottom'
                    renderTabBar={() => <ScrollableTabBar style={{ borderBottomWidth: 0}}/>}
                    style={{ borderBottomWidth: 0,overflow:'hidden', marginLeft:30,marginRight:10, position: 'relative', minHeight: 200, margin:0, padding:0 }}
                    tabBarTextStyle={{ fontSize: 14}}
                    tabBarUnderlineStyle ={{borderBottomWidth:1}}
                  >
                    <TabsContent1 tabLabel="Details" />
                    <TabsContent1 tabLabel="Amenities" />
                    <TabsContent1 tabLabel="Rooms" />
                  </ScrollableTabView>
                </View>

              </LinearGradient>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }

  // USING navigationOptions TO SET HEADER OF SCREEN
  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  }


};

class TabsContent1 extends Component {

  renderViewMore = (onPress) => {
    return (
      <Text onPress={onPress} style={{ textAlign: 'right', paddingTop: 5, paddingRight: 5, paddingBottom:10, color: '#c3c3c3' }}>Read more</Text>
    )
  }
  renderViewLess = (onPress) => {
    return (
      <Text onPress={onPress} style={{ textAlign: 'right', paddingTop: 5, paddingRight: 5, paddingBottom:10, color: '#c3c3c3' }}>Read less</Text>
    )
  }

  render() {
    return (
      <ScrollView>
        <ViewMoreText
          numberOfLines={3}
          renderViewMore={this.renderViewMore}
          renderViewLess={this.renderViewLess}
          textStyle={{ textAlign: 'left' }}
          style={{ flex: 1,}}
        >
          <Text style={{ textAlign: "left",  paddingLeft:30, paddingRight:10  }} >Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>

        </ViewMoreText>
      </ScrollView>

    )
  }
}

const styles = StyleSheet.create({
  containerscroll: {
    padding: 15,
    paddingLeft: 0,
    paddingRight: 0,
  },
  contentContainer: {
    paddingTop: 20,
    backgroundColor:'#e7ecf4',
    paddingLeft:10,
    minHeight: screenHeight
  },
  welcomeContainer: {
    alignItems: 'center',
    width:'100%',
    backgroundColor:'#f2f2f2',
    height:'100%'
  },
  textStyle: {
    fontWeight: 'bold',
    color: '#727272'
  },

  product_allView: {
    position: 'relative',
    height: 250,
    width: '100%',
  },
  product_location: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 3,
    paddingBottom: 3,
    backgroundColor: '#fe7965',
    color: '#fff',
    borderRadius: 2,
    fontSize: 16
  },
  propertyPrice: {
    textAlign: 'right',
    fontSize: 20,
    fontWeight: 'bold'
  },
  propertyAddressView: {
    justifyContent: 'flex-start',
    paddingLeft: 20
  },
  propertybookmarkView: {
    flex: 1,
    justifyContent: 'center'
  },
  propertyAddress: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  propertyamenities: {
    color: '#c2c2c4',
    fontSize: 14,
    fontWeight: 'bold'
  },
  product_detail: {
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
          height: 0,
          width: 0
        },
      },
      android: {
        elevation: 10,
      },
    }),
    borderRadius: 5,
    marginTop: -50,
    width: '90%',
    padding: 10,
    position: 'relative',

  },

  backgroundContainer: {
    position: 'absolute',
    height: 250,
    width: '100%'
  },
  bookmarIcon: {
    width: 32,
    height: 32,
    alignSelf: 'flex-end',
    marginRight: '5%',
    marginTop: '5%',
    backgroundColor: 'transparent'
  },
  favicon: {
    width: 24,
    height: 24,
    alignSelf: 'flex-end'
  },
  backbutton: {
    width: 16,
    height: 16,
    alignSelf: 'flex-start',
    marginLeft: '5%',
    marginTop: '5%',
    backgroundColor: 'transparent',
    position:'absolute'
  },
  backdrop: {
    flex: 1,
    width: '100%',
    height: '100%'
  },

});
